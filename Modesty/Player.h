//
//  Player.h
//  Modesty
//
//  Created by Josh on 12/24/13.
//  Copyright (c) 2013 Jukaela Enterprises. All rights reserved.
//

#import <Foundation/Foundation.h>

/**
 *  Data storage class that holds information about each user
 */
@interface Player : NSObject

/**
 *  The user's username
 */
@property (strong, nonatomic) NSString *username;

@end
